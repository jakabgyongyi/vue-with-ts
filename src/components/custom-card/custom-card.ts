import { Component, Emit, Prop, Vue } from "vue-property-decorator";

@Component
export default class CustomCard extends Vue {
  /**
   * Props
   */
  @Prop({ required: false, default: "PropA default" }) readonly propA!: string;
  @Prop({ required: true }) readonly propB!: number;
  @Prop({ default: () => ({ lastName: "Baby", firstName: "Yoda" }) })
  readonly propC: unknown;
  @Prop({ required: true }) readonly componentIdentifier!: string;

  /**
   * Methods
   */
  /**
   * The event name specified as emit decorator argument is the event that automatically gets fired/emitted at the end of this handler
   * componentId is the argument that is used when event is emitted
   * To have more control over how the argument are passed just use this.$emit(<event-name-with-kebab-case>, args...)
   * If you emit event with kebab-case, reference with kebab-case
   */
  @Emit("click-from-child")
  private handleClick(componentId: string): void {
    console.log(
      "This is the handler for click event in custom card component: " +
        componentId
    );
  }
}
