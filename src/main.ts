import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
//If you want to use any element from this framework import it here and use it whenever you want (to avoid multiple imports)
import {
  Button,
  Card,
  Row,
  Col,
  Switch,
  Divider,
  Input,
  Slider,
  Form,
  FormItem,
  Progress,
} from "element-ui";
import "element-ui/lib/theme-chalk/index.css";

Vue.config.productionTip = false;

Vue.use(Button);
Vue.use(Card);
Vue.use(Row);
Vue.use(Col);
Vue.use(Switch);
Vue.use(Divider);
Vue.use(Input);
Vue.use(Slider);
Vue.use(Form);
Vue.use(FormItem);
Vue.use(Progress);

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
