export class User {
  private id: number;
  private firstName: string;
  private lastName: string;
  private age: number;

  /**
   * In order to autogenerate Typescript custructors, gettern and setters add Typescript Toolbox from marketplace
   * Press Ctrl + Shift + P and search in the cursor Constructor, Getter or Setter
   */

  constructor(
    $id: number,
    $firstName: string,
    $lastName: string,
    $age: number
  ) {
    this.id = $id;
    this.firstName = $firstName;
    this.lastName = $lastName;
    this.age = $age;
  }

  /**
   * Getter $firstName
   * @return {string}
   */
  public get $firstName(): string {
    return this.firstName;
  }

  /**
   * Setter $firstName
   * @param {string} value
   */
  public set $firstName(value: string) {
    this.firstName = value;
  }

  /**
   * Getter $lastName
   * @return {string}
   */
  public get $lastName(): string {
    return this.lastName;
  }

  /**
   * Setter $lastName
   * @param {string} value
   */
  public set $lastName(value: string) {
    this.lastName = value;
  }

  /**
   * Getter $age
   * @return {number}
   */
  public get $age(): number {
    return this.age;
  }

  /**
   * Setter $age
   * @param {number} value
   */
  public set $age(value: number) {
    this.age = value;
  }

  /**
   * Getter $id
   * @return {number}
   */
  public get $id(): number {
    return this.id;
  }

  /**
   * Setter $id
   * @param {number} value
   */
  public set $id(value: number) {
    this.id = value;
  }
}
