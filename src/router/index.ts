import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";
import Home from "@/views/Home.vue";
import ParentChild from "@/views/component-relation/ParentChild.vue";
import Directives from "@/views/directives/Directives.vue";
import NestedRouting from "@/views/nested-routing/NestedRouting.vue";
import NestedChildOne from "@/views/nested-routing/NestedChildOne.vue";
import NestedChildTwo from "@/views/nested-routing/NestedChildTwo.vue";
import LifeCycleHooks from "@/views/life-cycle/LifeCycleHooks.vue";
import ComputedPropsWatchers from "@/views/computed-and-watchers/ComputedPropsWatchers.vue";
import StateManagement from "@/views/state-management/StateManagement.vue";

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/parent-child",
    name: "ParentChild",
    component: ParentChild,
  },
  {
    path: "/directives",
    name: "Directives",
    component: Directives,
  },
  {
    path: "/nested-routing",
    name: "NestedRouting",
    component: NestedRouting,
    children: [
      {
        path: "child-one",
        name: "NestedChildOne",
        component: NestedChildOne,
      },
      {
        path: "child-two",
        name: "NestedChildTwo",
        component: NestedChildTwo,
      },
    ],
  },
  {
    path: "/life-cycle-hooks",
    name: "LifeCycleHooks",
    component: LifeCycleHooks,
  },
  {
    path: "/comp-prop-watchers",
    name: "ComputedPropsWatchers",
    component: ComputedPropsWatchers,
  },
  {
    path: "/state-management",
    name: "StateManagement",
    component: StateManagement,
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
