import Vue from "vue";
import Vuex from "vuex";
import ModuleOne from "@/store/modules/modules-one";
import ModuleTwo from "@/store/modules/modules-two";

Vue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    ModuleOne,
    ModuleTwo,
  },
});

export default store;
