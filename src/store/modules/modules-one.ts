import { VuexModule, Module, Mutation, Action } from "vuex-module-decorators";

@Module({ namespaced: true })
class ModuleOne extends VuexModule {
  /**
   * Internal state
   */
  public stateDataOne = "";
  public stateDataTwo = 0;
  public stateDataThree = false;

  /**
   * Mutations: the only way to change the internal state of a store
   * In simple vue context for each mutation the state is passed as argument, with decorators this is handled by the library
   */
  @Mutation
  public setStateDataOne(newValue: string): void {
    this.stateDataOne = newValue;
  }

  @Mutation
  public setStateDataTwo(newValue: number): void {
    this.stateDataTwo = newValue;
  }

  @Mutation
  public setStateDataThree(newValue: boolean): void {
    this.stateDataThree = newValue;
  }

  /**
   * Actions: the communication with the exterior of a store, in order to change store internal state
   * In simple vue context for each action the context is passed as argument, with decorators this is handled by the library
   * The mutation invocation remain the same as before: using the context commit the change on an action with an argument
   */
  @Action
  public updateStateDataOne(newValue: string): void {
    this.context.commit("setStateDataOne", newValue);
  }

  @Action
  public updateStateDataTwo(newValue: number): void {
    this.context.commit("setStateDataTwo", newValue);
  }

  @Action
  public updateStateDataThree(newValue: boolean): void {
    this.context.commit("setStateDataThree", newValue);
  }
}

export default ModuleOne;
