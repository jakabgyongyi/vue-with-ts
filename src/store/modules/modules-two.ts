import { VuexModule, Module, Mutation, Action } from "vuex-module-decorators";
@Module({ namespaced: true })
class ModuleTwo extends VuexModule {
  /**
   * Internal state
   */
  public firstName = "";
  public lastName = "";

  /**
   * Mutations: the only way to change the internal state of a store
   * In simple vue context for each mutation the state is passed as argument, with decorators this is handled by the library
   */
  @Mutation
  public setFirstName(newValue: string): void {
    this.firstName = newValue;
  }

  @Mutation
  public setLastName(newValue: string): void {
    this.lastName = newValue;
  }

  /**
   * Actions: the communication with the exterior of a store, in order to change store internal state
   * In simple vue context for each action the context is passed as argument, with decorators this is handled by the library
   * The mutation invocation remain the same as before: using the context commit the change on an action with an argument
   */
  @Action
  public updateFirstName(newValue: string): void {
    this.context.commit("setFirstName", newValue);
  }

  @Action
  public updateLastName(newValue: number): void {
    this.context.commit("setLastName", newValue);
  }
}

export default ModuleTwo;
