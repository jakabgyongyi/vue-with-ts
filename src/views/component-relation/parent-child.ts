import { Component, Vue } from "vue-property-decorator";
import CustomCard from "@/components/custom-card/CustomCard.vue";

@Component({
  components: { CustomCard },
})
export default class ParentChild extends Vue {
  /**
   * Data
   */
  private examplePropC1 = { lastName: "Solo", firstName: "Han" };
  private examplePropC2 = { lastName: "Skywalker", firstName: "Luke" };
  private lastChildEvent = "";

  /**
   * Methods
   */
  private handleClick(componentIdentifier: string): void {
    this.lastChildEvent = componentIdentifier;
  }
}
