import { Component, Vue, Watch } from "vue-property-decorator";
import { User } from "@/models/simple-user";

@Component
export default class ComputedPropsWatchers extends Vue {
  /**
   * Data
   */
  private user = new User(new Date().getTime(), "", "", 0);
  private userList: Array<User> = [];
  private progressPercentage = 0;
  private inWaiting = false;

  /**
   * Computed proprties
   * Suggestion: Use computed properties when you want a new value derived from the underlying value/values.
   */
  get fullName(): string {
    return this.user.$lastName + " " + this.user.$firstName;
  }

  get adultUsers(): Array<User> {
    return this.userList.filter(function (user: User) {
      return user.$age > 18;
    });
  }

  /**
   * Methods
   */
  public onSubmit(): void {
    this.progressPercentage = 10;
    this.inWaiting = true;
  }

  /**
   * In order for the watch handler to be triggered, the value has to be changed.
   * The timeout is set in order to have control over the change rate of percentage value.
   * Suggestion: Use watch when you want to attach a side effect to the change of underlying value/values.
   */
  @Watch("progressPercentage")
  changeProgressPercentage(): void {
    if (this.inWaiting) {
      setTimeout(() => {
        if (this.progressPercentage < 100) {
          this.progressPercentage += 10;
        } else {
          this.userList.push(this.user);
          this.user = new User(new Date().getTime(), "", "", 0);
          this.progressPercentage = 0;
          this.inWaiting = false;
        }
      }, 300);
    }
  }
}
