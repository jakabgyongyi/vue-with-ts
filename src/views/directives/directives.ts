import { Component, Vue } from "vue-property-decorator";
import CustomCard from "@/components/custom-card/CustomCard.vue";

@Component({
  components: { CustomCard },
})
export default class Directives extends Vue {
  /**
   * Data
   */
  private vIfToggle = false;
  private vShowToggle = false;
  private exampleListOne = [
    { name: "banana", color: "yellow" },
    { name: "apple", color: "green" },
    { name: "blueberry", color: "blue" },
  ];
  private exampleListTwo = [
    { name: "banana", color: "yellow" },
    { name: "apple", color: "green" },
    { name: "blueberry", color: "blue" },
    { name: "mellon", color: "red" },
  ];
  private exampleTwoNameInput = "";
  private exampleTwoColorInput = "";
  private exampleListThree = [
    { componentId: "Child1", propA: "Child1 propA", propB: 1 },
    { componentId: "Child2", propA: "Child2 propA", propB: 2 },
    { componentId: "Child3", propA: "Child3 propA", propB: 3 },
  ];
  private exampleListFour = [
    { id: 0, name: "Anna" },
    { id: 1, name: "Makro" },
    { id: 2, name: "Flora" },
    { id: 3, name: "Kristof" },
  ];
  private vForToggle = false;

  /**
   * Methods
   */
  private addElementToList(): void {
    this.exampleListTwo.push({
      name: this.exampleTwoNameInput,
      color: this.exampleTwoColorInput,
    });
    this.exampleTwoNameInput = "";
    this.exampleTwoColorInput = "";
  }

  /**
   * Computed properties
   */
  get filteredList(): Array<unknown> {
    return this.exampleListFour.filter(function (item) {
      return item.id % 2 == 0;
    });
  }
}
