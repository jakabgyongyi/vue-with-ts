import { Component, Vue } from "vue-property-decorator";
import CustomCard from "@/components/custom-card/CustomCard.vue";

@Component({
  components: { CustomCard },
})
export default class LifeCycleHooks extends Vue {
  /**
   * Data
   */
  // Data or properties that have no initial value have to declare non-null operator (!)
  private listData: Array<unknown> = [];
  private toggle = false;

  /**
   * Lifecycle hooks
   */
  created(): void {
    console.log("Created hook fired");
    this.listData.push({
      componentId: "Child1",
      propA: "PropA child1",
      propB: 1,
    });
    this.listData.push({
      componentId: "Child2",
      propA: "PropA child2",
      propB: 2,
    });
  }

  mounted(): void {
    console.log("Mounted hook fired");
    this.listData.push({
      componentId: "Child3",
      propA: "PropA child3",
      propB: 3,
    });
    this.listData.push({
      componentId: "Child4",
      propA: "PropA child4",
      propB: 4,
    });
  }

  // Because data is added in created or mounter hooks, updated hook will be fired to rerender parts of the DOM
  updated(): void {
    console.log("Updated hook fired");
  }

  // Fired when we route/navigate away from a view
  destroyed(): void {
    console.log("Destoyed hook fired");
  }

  /**
   * Methods
   */
  filteredListMethod(listData: Array<unknown>): Array<unknown> {
    console.log("Evaluated list filter method");
    return listData.filter((item: any) => item.propB % 2 == 0);
  }

  /**
   * Computed properties
   */
  get filteredList(): Array<unknown> {
    console.log("Evaluated list computed property");
    return this.listData.filter((item: any) => item.propB % 2 == 0);
  }
}
