import { Component, Vue } from "vue-property-decorator";
import CustomCardInput from "@/components/custom-card-module-one/CustomCardInput.vue";
import CustomCardInputTwo from "@/components/custom-card-module-one/CustomCardInputTwo.vue";
import CustomCardOutput from "@/components/custom-card-module-one/CustomCardOutput.vue";
import CustomCardDepthOne from "@/components/custom-card-module-two/CustomCardDepthOne.vue";
import { namespace } from "vuex-class";

const moduleOne = namespace("ModuleOne");
const moduleTwo = namespace("ModuleTwo");
@Component({
  components: {
    CustomCardInput,
    CustomCardInputTwo,
    CustomCardOutput,
    CustomCardDepthOne,
  },
})
export default class StateManagement extends Vue {
  @moduleTwo.State
  public firstName!: string;
  @moduleTwo.State
  public lastName!: string;
  @moduleOne.State
  public stateDataTwo!: number;
}
